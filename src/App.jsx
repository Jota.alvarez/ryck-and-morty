
import './App.css'
import {Home} from './components/pages/Home'
import {Personaje} from './components/pages/Personaje'
import { Header } from './components/navBar/Header'
import { Routes, Route, Link } from 'react-router-dom';


function App() {

  return (
    <div className="App">
      <Header />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/personaje/:personajeId" element={<Personaje />} />
      </Routes>
    </div>
  );
}

export default App
