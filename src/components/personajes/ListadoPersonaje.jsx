import { PersonajeItem } from "./PersonajeItem";
import { useEffect, useState } from "react";
import axios from "axios";

export function ListadoPersonaje({buscador}) {

    const[personajes, setPersonajes] = useState(null);
    
    let personajesFiltrados = personajes?.results

    if (buscador && personajes) {
        personajesFiltrados = personajes.results.filter((personaje) => {
            let nombreMinuscula = personaje.name.toLowerCase();
            let busquedaMisnuscula = buscador.toLowerCase();
            return nombreMinuscula.includes(busquedaMisnuscula);
        })
    }

   

    useEffect(() => {
        axios.get('https://rickandmortyapi.com/api/character')
        .then((respuesta) => {
            setPersonajes(respuesta.data)
        })
    },[]);
    

    return(
        <div className="row " style={{margin:'0 4em', backgroundColor: 'hsla(220, 13%, 18%, 0.66)'}}>
            {personajesFiltrados

            ? personajesFiltrados.map((personaje) => {
                return <PersonajeItem key={personaje.id} {...personaje}/>
            }) 
            :'Cargando Personajes...'}
        </div>
    );
}