import {Link} from 'react-router-dom'

const stylepersonaje = {
    maxWidth: "540px", 
    borderRadius:'25px',
    border: '1px solid white',
    boxShadow:'2px 6px 8px  rgb(153, 149, 149)',
    overflow:'hidden'                
}

function getEstiloStatus(status) {

    let color = 'green';

    if (status == 'unknown'){
        color = 'gray';
    } 

    if (status == 'Dead') {
        color = 'red'
    }


    const estiloCirculo = {
        color: color,   
    };
    return estiloCirculo;
}



export function PersonajeItem({
    id,
    name,
    status, 
    species, 
    location, 
    image, 
    origin
    }) {
    
    return(
        <div className="col-3 py-2">
            <Link to={`/personaje/${id}`} style={{textDecoration:'none', color:'initial'}}>
            <div className="card mb-3" 
            style={stylepersonaje}
                    >
                <div className="row g-0">
                    <div className="col-md-4">
                    <img src={image} className="img-fluid rounded-end" alt={name}
                        style={{height:'100%', 
                        objectFit:'cover'
                        }}/>
                    </div>
                    <div className="col-md-8" >
                        <div className="card-body" style={{textAlign:'start'}}>
                            <h5 className="card-title mb-0" style={{fontSize:'1em'}} >{name}</h5>
                            <p className="py-2"style={getEstiloStatus(status)}>
                            <span ></span>
                            {status} - {species}  
                            </p>

                            <p className="mb-0 text-muted "style={{fontSize:'.9em'}} >Last unknow location: </p>
                            <p style={{fontSize:'1em'}}>{location?.name}</p>

                            <p className="mb-0 text-muted " style={{fontSize:'.9em'}} >Origin: </p>
                            <p style={{fontSize:'1em'}}>{origin?.name}</p>
                        </div>
                    </div>
                </div>
            </div> 
            </Link>
        </div>
    );
}