
function getEstiloStatus(status) {

    let color = 'green';

    if (status == 'unknowm'){
        color = 'gray';
    } 

    if (status == 'Dead') {
        color = 'red'
    }

    const estiloCirculo = {
        color: color,
        marginTop:'12px',
        fontSize:'1.5em'
    };
    return estiloCirculo;
}

export function BannerDetallePersonaje({ 
    id,
    name,
    status, 
    species, 
    location, 
    image, 
    origin
}) {

    return(
        <div className="card mb-3" 
            style={{
                    maxWidth: "860px",
                    height:'450px', 
                    margin:'1em auto',
                    borderRadius:'25px',
                    border: '1px solid gray ',
                    boxShadow:'2px 4px 6px rgba(30, 30, 34, 0.4)',
                    overflow:'hidden'}}>
                <div className="row g-0">
                    <div className="col-md-6" style={{height:'100%'}}>
                    <img src={image} className="img-fluid rounded-end" alt={name}
                        style={{height:'450px', 
                        objectFit:'cover'
                        }}/>
                    </div>
                    <div className="col-md-6" >
                        <div className="card-body" style={{textAlign:'start', marginTop:'4em'}}>
                            <h5 className="card-title mb-0" style={{fontSize:'3em'}} >{name}</h5>
                            <p style={getEstiloStatus(status)}>
                                <span 
                                
                                ></span>
                            {status} - {species}  
                            </p>

                            <p className="mb-0 text-muted "style={{fontSize:'1em'}} >Last unknow location: </p>
                            <p style={{fontSize:'1.5em'}}>{location?.name}</p>

                            <p className="mb-0 text-muted " style={{fontSize:'1em'}} >Origin: </p>
                            <p style={{fontSize:'1.5em'}}>{origin?.name}</p>
                        </div>
                    </div>
                </div>
            </div>
    );
}