export function Buscador({valor, onValor}) {

    return(
        <div className="d-flex justify-content-end" >
            <div className="mb-5 col-6" >
                <input 
                value={valor}
                onChange={(evento) => onValor(evento.target.value)}
                type="text" 
                placeholder="Buscar personaje..." 
                className="form-control"
                style={{
                        width:'92%',
                        height:'3em', 
                        borderRadius:'15px',
                        border: '1px solid gray',
                        boxShadow:'2px 4px 6px  rgb(153, 149, 149)'
                    }}
                />
            </div>
        </div>
    );
}