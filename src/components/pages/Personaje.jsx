
import axios from "axios";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { BannerDetallePersonaje } from "../personajes/BannerDetallesPersonaje";
import {ListaEpisodios} from '../Episodio/ListaEpisodios'
import { Footer } from "../footer/Footer";

const styleText = {
  padding:'1em',
  fontSize:'5em',
  color:'#000',
  whidth:'',
  letterSpacing:'1.5px',
  textShadow:'0 0 6px 4px gray',
  
}

export function Personaje() {
  let { personajeId } = useParams();
  let [personaje, setPersonaje] = useState(null);
  let [episodios, setEpisodios] = useState(null);

  useEffect(() => {
    axios
      .get(`https://rickandmortyapi.com/api/character/${personajeId}`)
      .then((respuesta) => {
        setPersonaje(respuesta.data);
      });
  }, []);

  useEffect(() => {
    if (personaje) {
      let peticionesEpisodios = personaje.episode.map((episodio) => {
        return axios.get(episodio);
      });

      Promise.all(peticionesEpisodios).then((respuestas) => {
        setEpisodios(respuestas);
      });
    }
  }, [personaje]);

  return (
    <div className="col-12 px-4" style={{ height: "500px" }}>
      {personaje ? (
        <div className="col-12 px-4 container" style={{width:'102vw'}}>
          <BannerDetallePersonaje {...personaje} />
          <h2
          style={styleText}>Episodios</h2>
          {episodios ? (
            <ListaEpisodios episodios={episodios} />
          ) : (
            <div>Cargando Episodios...</div>
          )}
        </div>
      ) : (
        <div>Cargando Personaje...</div>
      )}
      <Footer />
    </div>
  );
}
