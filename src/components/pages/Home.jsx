import { Banner } from "../navBar/Banner";
import { Buscador } from "../input/Buscador";
import { ListadoPersonaje } from "../personajes/ListadoPersonaje";
import { useState } from "react";
import { Footer } from "../footer/Footer";

const styleTextHome = {
    fontSize:'5em',
    color:'#fff',
    letterSpacing:'1.5px',
    backgroundColor: 'hsla(220, 13%, 18%)',
    textShadow:'0 0 6px 4px white',
    margin: '2em 0 0  4em '
    
  }

export function Home() {

    let [buscar, setBuscar] =useState('');
    
 


    return(
        <div className="px-4" style={{backgroundColor: 'hsla(220, 13%, 18%)'}}>
            <Banner />
            <h2 className="text-start" style={styleTextHome}>Personajes</h2>
            
            <Buscador valor={buscar} onValor={setBuscar} />

            <ListadoPersonaje buscador={buscar} />

            <Footer />
        </div>
    );
}
