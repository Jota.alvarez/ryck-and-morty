import imgBanner from '../../assets/images/banner.jpg'

export function Banner() {

    return(
        <div>
            <img style={{width: '100%',objectFit:'cover', height:'92vh', margin:'-1.5em'}} 
            src={imgBanner} alt="banner" />
        </div>
    );
}