import { Link } from "react-router-dom";
import logoImg from "../../assets/images/logo.png"

const texto = {
    fontFamily: 'Roboto Serif sans-serif', 
    fontSize:'1.7em', 
    fontWeight:'400', 
    letterSpacing:'.5px',
    marginLeft:'10px'
}

export function Header() {

    return(
        <nav className="navbar navbar-expand-lg navbar-light bg-light sticky-top" style={{padding:'0' }}>
            <div className="container-fluid"style={{backgroundColor:'hsla(220, 13%, 18%)' ,padding:'10px 20px', borderBottom:'1px solid hsla(220, 13%, 30%)'}}>
                <Link
                    className="navbar-brand" to="/">
                    <img style={{width: '8em'}} src={logoImg} alt="Logo" />
                    </Link>
                <div className="collapse navbar-collapse" id="navbarText">
                    <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                        <li className="nav-item">
                        <Link className="nav-link active text-white" 
                        aria-current="page" 
                        to="/" 
                        style={texto}
                        >Home</Link>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    );
}