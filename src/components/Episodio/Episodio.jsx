
const styleCards={
    maxWidth: '18rem',
    borderRadius: '18px',
    opacity:'.9',
    boxShadow:'2px 6px 8px  rgb(153, 149, 149)'
}



export function Episodio({name, air_date, episode}) {

    return(
        <div className="col-3" >
            <div 
            className="card text-dark bg-light mb-3" 
            style={styleCards}>

                <div className="card-header">{name}</div>
                    <div className="card-body">
                        <h5 className="card-title">{air_date}</h5>
                        <p className="card-text">{episode}</p>
                    </div>
                </div>
        </div>
    );
}