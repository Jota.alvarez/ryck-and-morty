import footerImg from '../../assets/images/logo.png'


const colorFooter ={

    fontSize: '2em',
    color:'#fff',
    marginLeft:'-3em'
}

const styleFooter = {
    width: '100%',
    height: '46vh',
    marginTop:'1.5em',
    paddingBottom:'2em'
}
const styleImg = {
    width: '50%'
}

export function Footer() {

    return( 
        <div style={styleFooter}>
        <img 
        src={footerImg} 
        alt="Imagen footer" 
        style={styleImg}
        />
        <p style={colorFooter}>Derechos reservados  &copy; Proyecto creado por  Jhonatan Alvarez  " &#x2111; "  </p>
        <p style={colorFooter}>CAR IV - 2022</p>
        </div>
    );
}